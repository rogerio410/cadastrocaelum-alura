package br.com.caelum.cadastrocaelum;

import android.app.Activity;
import android.content.Intent;
import android.media.browse.MediaBrowser;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

import br.com.caelum.cadastrocaelum.dao.AlunoDAO;
import br.com.caelum.cadastrocaelum.dao.DBHelper;
import br.com.caelum.cadastrocaelum.modelo.Aluno;
import br.com.caelum.cadastrocaelum.modelo.FormularioAlunoHelper;

/**
 * Created by rogermac on 21/12/15.
 */
public class FormularioActivity extends Activity {

    FormularioAlunoHelper frmHelperAluno;
    private final DBHelper dbHelper = new DBHelper(this);
    String caminhoArquivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.formulario);

        Button botao = (Button) findViewById(R.id.botao);

        frmHelperAluno = new FormularioAlunoHelper(this);

        final Aluno alunoParaSerAlterado = (Aluno)getIntent().getSerializableExtra("alunoSelecionado");

        if (alunoParaSerAlterado != null){
            botao.setText("Alterar");
            frmHelperAluno.colocaAlunoNoFormulario(alunoParaSerAlterado);
        }
        //Toast.makeText(this, "Aluno selecionado " + alunoParaSerAlterado, Toast.LENGTH_SHORT).show();

        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Aluno aluno = frmHelperAluno.pegaAlunoDoFormulario();
                AlunoDAO alunoDAO = new AlunoDAO(dbHelper);

                if (alunoParaSerAlterado != null){
                    aluno.setId(alunoParaSerAlterado.getId());
                    alunoDAO.atualiza(aluno);
                }else {
                    alunoDAO.insere(aluno);
                }

                Toast.makeText(FormularioActivity.this, "Dados Salvos: " + aluno.getNome(), Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        ImageView imagem = frmHelperAluno.getFoto();

        imagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent irParaCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                caminhoArquivo = getExternalFilesDir(null)+"/"+System.currentTimeMillis()+".png";
                File arquivo = new File(caminhoArquivo);

                Uri localFoto = Uri.fromFile(arquivo);
                irParaCamera.putExtra(MediaStore.EXTRA_OUTPUT, localFoto);

                startActivityForResult(irParaCamera, 123);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 123){
            if (resultCode == Activity.RESULT_OK){
                frmHelperAluno.carregaImagem(caminhoArquivo);
            }else{
                caminhoArquivo = null;
            }

        }
    }
}
