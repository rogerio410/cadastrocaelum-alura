package br.com.caelum.cadastrocaelum;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ListActivity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import br.com.caelum.cadastrocaelum.adapter.AlunoAdapter;
import br.com.caelum.cadastrocaelum.converter.AlunoConverter;
import br.com.caelum.cadastrocaelum.dao.AlunoDAO;
import br.com.caelum.cadastrocaelum.dao.DBHelper;
import br.com.caelum.cadastrocaelum.fragment.ListaProvaFragment;
import br.com.caelum.cadastrocaelum.modelo.Aluno;
import br.com.caelum.cadastrocaelum.support.WebClient;
import br.com.caelum.cadastrocaelum.task.EnviaContatosTask;

/**
 * Created by rogermac on 19/12/15.
 */
public class ListaAlunosActivity extends Activity {

    ListView lista;
    private final DBHelper dBhelper = new DBHelper(this);
    Aluno aluno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listagem_alunos);

        lista = (ListView)findViewById(R.id.lista);

        registerForContextMenu(lista);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                //Toast.makeText(ListaAlunosActivity.this, "A posição é " + position, Toast.LENGTH_SHORT).show();

                Aluno aluno = (Aluno)adapter.getItemAtPosition(position);

                Intent irParaFormulario = new Intent(ListaAlunosActivity.this, FormularioActivity.class);
                irParaFormulario.putExtra("alunoSelecionado", aluno);

                startActivity(irParaFormulario);

            }
        });

        lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapter, View view, int position, long id) {

                //Toast.makeText(ListaAlunosActivity.this, "Aluno clicado: " + adapter.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                aluno = (Aluno)adapter.getItemAtPosition(position);
                return false;
            }
        });

    }



    @Override
    protected void onResume() {
        super.onResume();

        recarregarAlunos();
    }

    private void recarregarAlunos() {
        AlunoDAO alunoDAO = new AlunoDAO(dBhelper);
        List<Aluno> alunos = alunoDAO.getAlunos();

        //ArrayAdapter<Aluno> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, alunos);
        AlunoAdapter adapter = new AlunoAdapter(alunos, this);

        lista.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add("Novo");

        getMenuInflater().inflate(R.menu.menu_lista_alunos, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.novo:
                Intent irParaFormulario = new Intent(this, FormularioActivity.class);
                startActivity(irParaFormulario);
                break;
            case R.id.enviar_alunos:
                EnviaContatosTask ect = new EnviaContatosTask(this, dBhelper);
                ect.execute();
                break;
            case R.id.receber_alunos:
                Intent intentProvas = new Intent(this, ProvasActivity.class);
                startActivity(intentProvas);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuItem deletar = menu.add("Deletar");
        MenuItem ligar = menu.add("Ligar");
        MenuItem site = menu.add("Navegar no Site");
        MenuItem sms = menu.add("Enviar SMS");
        MenuItem mapa = menu.add("Ver no mapa");

        deletar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                AlunoDAO alunoDAO = new AlunoDAO(dBhelper);
                alunoDAO.deletar(aluno);
                recarregarAlunos();

                Toast.makeText(ListaAlunosActivity.this, "Aluno " + aluno.getNome()+" removido!", Toast.LENGTH_SHORT).show();

                return false;
            }
        });


        //Ação ligar não está funcionando, No Andoid 6, devido a mudanças nas permissoes do Android.
        ligar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (ActivityCompat.checkSelfPermission(ListaAlunosActivity.this, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED){

                    ActivityCompat.requestPermissions(ListaAlunosActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 0);

                }else {

                    Intent irParaTelaDiscagem = new Intent(Intent.ACTION_CALL);

                    Uri telefoneAluno = Uri.parse("tel:"+aluno.getFone());
                    irParaTelaDiscagem.setData(telefoneAluno);

                    startActivity(irParaTelaDiscagem);

                }




                return false;
            }
        });

        site.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                Intent abrirSite = new Intent(Intent.ACTION_VIEW);
                Uri enderecoURL = Uri.parse("http://"+aluno.getSite());

                abrirSite.setData(enderecoURL);
                startActivity(abrirSite);

                return false;
            }
        });

        sms.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                Intent enviarSMS = new Intent(Intent.ACTION_VIEW);

                Uri smsAluno = Uri.parse("sms:"+aluno.getFone());

                enviarSMS.setData(smsAluno);
                enviarSMS.putExtra("sms_body", "Olá " + aluno.getNome() + "! seja bem-vindo!");

                startActivity(enviarSMS);

                return false;
            }
        });

        mapa.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                Intent verNoMapa = new Intent(Intent.ACTION_VIEW);
                Uri enderecoMap = Uri.parse("geo:0,0?z=14&q="+aluno.getEndereco());

                verNoMapa.setData(enderecoMap);

                startActivity(verNoMapa);

                return false;
            }
        });
    }


}
