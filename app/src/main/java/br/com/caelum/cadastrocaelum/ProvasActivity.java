package br.com.caelum.cadastrocaelum;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;

import br.com.caelum.cadastrocaelum.fragment.ListaProvaFragment;

/**
 * Created by rogermac on 09/01/16.
 */
public class ProvasActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_provas);

        FragmentTransaction tx = getFragmentManager().beginTransaction();
        tx.replace(R.id.provas_frag, new ListaProvaFragment());
        tx.commit();
    }
}
