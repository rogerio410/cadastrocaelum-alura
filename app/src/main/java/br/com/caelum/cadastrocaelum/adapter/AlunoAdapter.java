package br.com.caelum.cadastrocaelum.adapter;

import android.app.Activity;
import android.app.Notification;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import br.com.caelum.cadastrocaelum.ListaAlunosActivity;
import br.com.caelum.cadastrocaelum.R;
import br.com.caelum.cadastrocaelum.modelo.Aluno;

/**
 * Created by rogermac on 27/12/15.
 */
public class AlunoAdapter extends BaseAdapter{


    private List<Aluno> alunos;
    private Activity activity;

    public AlunoAdapter(List<Aluno> alunos, Activity activity) {
        this.alunos = alunos;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return this.alunos.size();
    }

    @Override
    public Object getItem(int position) {
        return this.alunos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.alunos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View linha = inflater.inflate(R.layout.item, null);

        Aluno aluno = this.alunos.get(position);

        TextView nome = (TextView)linha.findViewById(R.id.nome);
        nome.setText(aluno.getNome());

        TextView fone = (TextView)linha.findViewById(R.id.fone);
        fone.setText(aluno.getFone());

        TextView endereco = (TextView)linha.findViewById(R.id.endereco);
        endereco.setText(aluno.getEndereco());

        ImageView foto = (ImageView)linha.findViewById(R.id.foto);

        if (aluno.getCaminhoFoto() != null){
            Bitmap image = BitmapFactory.decodeFile(aluno.getCaminhoFoto());
            Bitmap imagemReduzida = Bitmap.createScaledBitmap(image, 80, 80, true);

            foto.setImageBitmap(imagemReduzida);
        }else {
            foto.setImageResource(R.drawable.no_foto);
        }

        //Se com tela Horizontal
        TextView site = (TextView) linha.findViewById(R.id.site);

        if (site != null){
            site.setText(aluno.getSite());
        }

        return linha;
    }
}
