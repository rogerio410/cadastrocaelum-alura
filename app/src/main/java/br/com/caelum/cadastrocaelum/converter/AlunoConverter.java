package br.com.caelum.cadastrocaelum.converter;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONStringer;

import java.util.List;

import br.com.caelum.cadastrocaelum.modelo.Aluno;

/**
 * Created by rogermac on 01/01/16.
 */
public class AlunoConverter {

    public String toJSON(List<Aluno> alunos){

        JSONStringer json = new JSONStringer();
        try {
            json.object().key("list").array();
            json.object().key("aluno").array();
            for (Aluno aluno : alunos){
                json.object().key("nome").value(aluno.getNome())
                        .key("nota").value(aluno.getNota());
                json.endObject();
            }
            json.endArray().endObject().endArray().endObject();
            Log.i("Aluno", json.toString());
            return json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }
}
