package br.com.caelum.cadastrocaelum.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.cadastrocaelum.modelo.Aluno;

/**
 * Created by rogermac on 21/12/15.
 */
public class AlunoDAO{


    private static final String TABELA = "Aluno";

    private final DBHelper helper;

    public AlunoDAO(DBHelper helper){
       this.helper = helper;
    }

    public void insere(Aluno a){

        ContentValues cv = AlunoToContentValue(a);

        helper.getWritableDatabase().insert(TABELA, null, cv);

    }

    private ContentValues AlunoToContentValue(Aluno a) {
        ContentValues cv = new ContentValues();
        cv.put("nome", a.getNome());
        cv.put("site", a.getSite());
        cv.put("endereco", a.getEndereco());
        cv.put("fone", a.getFone());
        cv.put("nota", a.getNota());
        cv.put("caminhoFoto", a.getCaminhoFoto());
        return cv;
    }

    public List<Aluno> getAlunos() {
        List<Aluno> alunos = new ArrayList<Aluno>();
        String sql = "SELECT * FROM "+TABELA+";";
        Cursor c = helper.getReadableDatabase().rawQuery(sql, null);

        while (c.moveToNext()){
            Aluno a = new Aluno();
            a.setId(c.getLong(c.getColumnIndex("id")));
            a.setNome(c.getString(c.getColumnIndex("nome")));
            a.setSite(c.getString(c.getColumnIndex("site")));
            a.setEndereco(c.getString(c.getColumnIndex("endereco")));
            a.setFone(c.getString(c.getColumnIndex("fone")));
            a.setNota(c.getDouble(c.getColumnIndex("nota")));
            a.setCaminhoFoto(c.getString(c.getColumnIndex("caminhoFoto")));
            alunos.add(a);
        }
        this.helper.close();
        return alunos;
    }

    public void deletar(Aluno aluno) {
        String[] args = {aluno.getId().toString()};
        this.helper.getWritableDatabase().delete(TABELA, "id = ?", args);
        this.helper.close();
    }

    public void atualiza(Aluno aluno) {
        ContentValues values = AlunoToContentValue(aluno);
        String[] args = {aluno.getId().toString()};
        this.helper.getWritableDatabase().update(TABELA, values, "id = ?", args);
        this.helper.close();
    }
}
