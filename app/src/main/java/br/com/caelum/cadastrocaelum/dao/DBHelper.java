package br.com.caelum.cadastrocaelum.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by rogermac on 22/12/15.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE = "CadastroCaelum";
    private static final int VERSAO = 2;
    private static final String TABELA_ALUNO = "Aluno";

    public DBHelper(Context context){
        super(context, DATABASE, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE "+TABELA_ALUNO+" (" +
                "id INTEGER PRIMARY KEY, " +
                "nome TEXT UNIQUE NOT NULL, " +
                "fone TEXT, " +
                "site TEXT, " +
                "endereco TEXT, " +
                "nota REAL, " +
                "caminhoFoto TEXT);";

        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS "+TABELA_ALUNO;
        db.execSQL(sql);
        onCreate(db);
    }
}
