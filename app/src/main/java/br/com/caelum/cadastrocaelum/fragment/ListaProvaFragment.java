package br.com.caelum.cadastrocaelum.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Arrays;
import java.util.List;

import br.com.caelum.cadastrocaelum.R;
import br.com.caelum.cadastrocaelum.modelo.Prova;

/**
 * Created by rogermac on 09/01/16.
 */
public class ListaProvaFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_provas_list, container, false);

        ListView listaProvas = (ListView)layout.findViewById(R.id.lista_provas);

        Prova p1 = new Prova("17/03/2016", "Matematica");
        Prova p2 = new Prova("17/03/2016", "Geografia");

        List<Prova> provas = Arrays.asList(p1, p2);

        ArrayAdapter<Prova> provaArrayAdapter = new ArrayAdapter<Prova>(getActivity(), android.R.layout.simple_list_item_1, provas);

        listaProvas.setAdapter(provaArrayAdapter);


        return layout;

    }
}
