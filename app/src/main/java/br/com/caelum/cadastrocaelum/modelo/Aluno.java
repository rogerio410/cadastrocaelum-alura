package br.com.caelum.cadastrocaelum.modelo;

import java.io.Serializable;

/**
 * Created by rogermac on 21/12/15.
 */
public class Aluno implements Serializable{

    private Long id;
    private String nome;
    private String site;
    private String fone;
    private String endereco;
    private Double nota;
    private String caminhoFoto;

    public Aluno() {
    }

    public static Aluno createAluno(String nome, String site, String fone, String endereco, Double nota, String caminhoFoto) {
        Aluno a = new Aluno();
        a.setNome(nome);
        a.setSite(site);
        a.setEndereco(endereco);
        a.setFone(fone);
        a.setNota(nota);
        a.setCaminhoFoto(caminhoFoto);
        return a;
    }

    @Override
    public String toString() {
        return this.id + " > "+this.nome + "  " +this.getFone() ;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Double getNota() {
        return nota;
    }

    public void setNota(Double nota) {
        this.nota = nota;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaminhoFoto() {
        return caminhoFoto;
    }

    public void setCaminhoFoto(String caminhoFoto) {
        this.caminhoFoto = caminhoFoto;
    }
}
