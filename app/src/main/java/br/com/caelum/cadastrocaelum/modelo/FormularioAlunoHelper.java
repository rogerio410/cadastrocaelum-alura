package br.com.caelum.cadastrocaelum.modelo;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import br.com.caelum.cadastrocaelum.FormularioActivity;
import br.com.caelum.cadastrocaelum.R;

/**
 * Created by rogermac on 21/12/15.
 */
public class FormularioAlunoHelper {

    EditText editnome;
    EditText editsite;
    EditText editendereco;
    EditText editfone;
    RatingBar ratingNota;
    private ImageView foto;
    Aluno aluno;

    public FormularioAlunoHelper(Activity activity){

        editnome = (EditText)activity.findViewById(R.id.nome);
        editsite = (EditText)activity.findViewById(R.id.site);
        editendereco = (EditText)activity.findViewById(R.id.endereco);
        editfone = (EditText)activity.findViewById(R.id.fone);

        ratingNota = (RatingBar)activity.findViewById(R.id.nota);
        foto = (ImageView)activity.findViewById(R.id.foto);

        aluno = new Aluno();


    }

    public Aluno pegaAlunoDoFormulario(){

        String nome = editnome.getText().toString();
        String site = editsite.getText().toString();
        String endereco = editendereco.getText().toString();
        String fone = editfone.getText().toString();

        double nota = ratingNota.getRating();

        Aluno alunoFormulario = Aluno.createAluno(nome, site, fone, endereco, nota, this.aluno.getCaminhoFoto());

        return alunoFormulario;
    }


    public void colocaAlunoNoFormulario(Aluno aluno) {

        this.aluno = aluno;

        editnome.setText(aluno.getNome());
        editsite.setText(aluno.getSite());
        editendereco.setText(aluno.getEndereco());
        editfone.setText(aluno.getFone());
        ratingNota.setRating(aluno.getNota().floatValue());

        if (this.aluno.getCaminhoFoto() != null){
            carregaImagem(this.aluno.getCaminhoFoto());
        }

    }

    public ImageView getFoto() {
        return foto;
    }

    public void carregaImagem(String caminhoArquivo) {
        aluno.setCaminhoFoto(caminhoArquivo);

        Bitmap image = BitmapFactory.decodeFile(caminhoArquivo);
        Bitmap imageReduzida = Bitmap.createScaledBitmap(image, 100,100,true);

        foto.setImageBitmap(imageReduzida);

    }
}
