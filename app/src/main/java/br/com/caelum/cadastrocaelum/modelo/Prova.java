package br.com.caelum.cadastrocaelum.modelo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rogermac on 09/01/16.
 */
public class Prova {

    private String data;
    private String materia;
    private List<String> topicos = new ArrayList<>();

    public Prova(String data, String materia){
        this.data = data;
        this.materia = materia;
    }

    @Override
    public String toString() {
        return this.data +" - "+ this.materia;
    }
}
