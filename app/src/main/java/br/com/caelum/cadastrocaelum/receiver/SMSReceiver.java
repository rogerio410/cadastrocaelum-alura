package br.com.caelum.cadastrocaelum.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created by rogermac on 27/12/15.
 */
public class SMSReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {

        Object[] mensagens = (Object[]) intent.getExtras().get("pdus");
        byte[] mensagem = (byte[]) mensagens[0];

        SmsMessage sms = SmsMessage.createFromPdu(mensagem);

        Toast.makeText(context, "SMS Recebida: " + sms.getDisplayMessageBody()+" de " +sms.getDisplayOriginatingAddress(), Toast.LENGTH_SHORT).show();
    }
}
