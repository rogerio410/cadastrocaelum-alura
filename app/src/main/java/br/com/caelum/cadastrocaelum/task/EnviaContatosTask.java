package br.com.caelum.cadastrocaelum.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import br.com.caelum.cadastrocaelum.R;
import br.com.caelum.cadastrocaelum.converter.AlunoConverter;
import br.com.caelum.cadastrocaelum.dao.AlunoDAO;
import br.com.caelum.cadastrocaelum.dao.DBHelper;
import br.com.caelum.cadastrocaelum.modelo.Aluno;
import br.com.caelum.cadastrocaelum.support.WebClient;

/**
 * Created by rogermac on 01/01/16.
 */
public class EnviaContatosTask extends AsyncTask<Object, Object, String> {

    private Context ctx;
    private DBHelper dbHelper;
    ProgressDialog progressDialog;

    public EnviaContatosTask(Context ctx, DBHelper dbHelper){

        this.ctx = ctx;
        this.dbHelper = dbHelper;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = ProgressDialog.show(ctx, "AGUARDE...", "Enviando informações para os servidor");
    }

    @Override
    protected String doInBackground(Object[] params) {

        AlunoConverter converter = new AlunoConverter();
        AlunoDAO alunoDAO = new AlunoDAO(dbHelper);
        List<Aluno> alunos = alunoDAO.getAlunos();

        String json = converter.toJSON(alunos);

        WebClient client = new WebClient("https://www.caelum.com.br/mobile");

        return client.post(json);
    }

    @Override
    protected void onPostExecute(String result) {
        progressDialog.dismiss();
        Toast.makeText(ctx, result, Toast.LENGTH_LONG).show();
    }
}
